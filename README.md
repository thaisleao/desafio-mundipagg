# Desafio Mundipagg

## Restrições
*Para executar o código, é necessário rodar os seguintes comandos:*

>1. *git clone https://bitbucket.org/thaisleao/desafio-mundipagg*
2. *cd desafio-mundipagg*
3. *npm install*
4. *gulp build*

* *O comando npm install instala todas as dependências do projeto;*
* *O comando gulp build roda o projeto e abre o navegador com a url http://localhost:9000/;*

## Funcionalidades entregues
* A página carrega todos os repositórios da empresa MundiPagg;
* Ao selecionar um dos repositórios, os dados são entregues e o gráfico gerado;
* A página é responsiva, adaptando-se facilmente a tablets e smartphones.

## Tecnologias
* *Automadizador Gulp*
* *SASS com metodologia BEM*
* *AngularJS 1.6*
