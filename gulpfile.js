var gulp = require('gulp'),
	sass = require('gulp-sass'),
	// angular = require('angular'),
	notify = require('gulp-notify'),
	livereload = require('gulp-livereload'),
	autoprefixer = require('gulp-autoprefixer'),
	jshint = require('gulp-jshint'),
	browserSync = require('browser-sync'),
	reload = browserSync.reload,
	del = require('del');

gulp.task('server', function() {
	livereload.listen();
    browserSync({
        port: 9000,
        server: {
            baseDir: ['app', 'dist']
        }
    });
});

gulp.task('sass', function(){
	return gulp.src(['app/css/main.scss'])
				.pipe(sass({ style: 'expanded' }))
				.pipe(gulp.dest('dist/css/'))
        		.pipe(reload({stream: true}));
				// .pipe(notify({message: 'Arquivo css compilado.'}));
});

gulp.task('jshint', function () {
    return gulp.src(['app/js/**/*.js', 'app/js/*.js'])
				.pipe(jshint())
				.pipe(jshint.reporter('default'));
});


gulp.task('clean', function() {
	return del(['dist/*']);
});

gulp.task('copy', function(){
	gulp.src('app/js/*.js')
		.pipe(gulp.dest('dist/js/'));

	
	gulp.src(['app/images/*.jpg', 'app/images/*.png', 'app/images/*.gif'])
		.pipe(gulp.dest('dist/images/'));

	gulp.src('app/*.html')
		.pipe(gulp.dest('dist/'));

});

gulp.task('watch', ['sass'], function(){

	gulp.watch(['app/css/*.scss'], ['sass']);

	livereload.listen();

	// gulp.watch(['dist/**']).on('change', livereload.changed);
});

gulp.task('default', ['clean', 'watch', 'jshint', 'sass', 'server']);

gulp.task('build', ['clean', 'server', 'sass', 'copy']);

