function trataValoresGrafico(commits) {

    var autores = [];
    var datas = [];
    var todosCommits = [];
    commits.forEach(function(c) {
        var data = formataData((c.commit.author.date).split('T')[0]);
        autores.push(c.commit.author.name);
        datas.push(data);
        var autorExiste = verificaSeExiste(todosCommits, c.commit.author.name, data);
        if (autorExiste === undefined) {
            todosCommits.push({
                nome: c.commit.author.name,
                data: data,
                email: c.commit.author.email,
                total: 1
            });
        } else {
            autorExiste.total = autorExiste.total + 1;
        }
    });

    autores = removeDuplicadas(autores);
    datas = removeDuplicadas(datas);

    geraDadosGrafico(autores.reverse(), datas.reverse(), todosCommits);

}
function removeDuplicadas(array) {
    return array.reduce(function(a, b) {
        if (a.indexOf(b) < 0) a.push(b);
        return a;
    }, []);
}

function verificaSeExiste(todosCommits, nome, data) {
    return todosCommits.find(x => x.nome === nome && x.data === data);
}

function formataData(data) {
    var mes = {
        '01': 'Jan',
        '02': 'Fev',
        '03': 'Mar',
        '04': 'Abr',
        '05': 'Mai',
        '06': 'Jun',
        '07': 'Jul',
        '08': 'Ago',
        '09': 'Set',
        '10': 'Out',
        '11': 'Nov',
        '12': 'Dez'
    };
    data = data.split('-');
    return mes[data[1]] + '/' + data[0];
}

function geraDadosGrafico(autores, datas, todosCommits) {

    var dadosGrafico = [];
    var dadosMes = [];
    datas.forEach(function(d) {
        dadosMes = [];
        dadosMes.push(d);
        autores.forEach(function(autor) {
            var valorExiste = verificaSeExiste(todosCommits, autor, d);
            if (valorExiste !== undefined) {
                dadosMes.push(valorExiste.total);
            } else {
                dadosMes.push(0);
            }
        });
        dadosGrafico.push(dadosMes);
    });
    gerarGrafico(dadosGrafico, autores);

}

function gerarGrafico(dadosGrafico, autores) {

    google.charts.load('current', { 'packages': ['line'] });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Mês/Ano');
        autores.forEach(function(autor) {
            data.addColumn('number', autor);
        });

        data.addRows(dadosGrafico);
        var options = {
            chart: {
                title: 'Commits',
                subtitle: 'Total de commits por repositório'
            },
            pointSize: 5,
            pointShape: 'circle',
            width: '100%',
            height: 600,
            chartArea: { left: 0, top: 0, width: "100%", height: "100%" }
        };

        var chart = new google.charts.Line(document.getElementById('grafico'));
        
        chart.draw(data, google.charts.Line.convertOptions(options));
        
    }
}

