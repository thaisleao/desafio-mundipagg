var app = angular.module('listaRepositorio', []);

app.controller('RepositorioController', ['$http', function($http) {
    var lista = this;
    lista.repositorios = [];
    lista.commits = [];
    
    $http.get('https://api.github.com/orgs/mundipagg/repos?access_token=27c8b2a01e38a7325f73bdac7114c8597633621c')
        .then(function(response) {
            lista.repositorios = response.data;
        }, function(error) {
            console.log(error);
        });
    
    this.getInfos = function(selecionado) {
        var urlCommits = selecionado.commits_url.split('{/sha}')[0]+'?access_token=27c8b2a01e38a7325f73bdac7114c8597633621c';
        var urlContribs = selecionado.contributors_url + '?access_token=27c8b2a01e38a7325f73bdac7114c8597633621c';
        $http.get(urlCommits)
            .then(function(response) {
                lista.commits = response.data;
                trataValoresGrafico(lista.commits);
            }, function(error) {
                lista.commits = [];
                console.log(error);
            });
        
        
        $http.get(urlContribs)
            .then(function(response) {
                lista.totalContribs = response.data.length;
            }, function(error) {
                lista.totalContribs = 0;
                console.log(error);
            });
    };

}]);